// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-database-interface>.
//
// Bot-Database-Interface is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, ConfigService } from "@mze9412-discord-bot-project/bot-runner";
import { Mutex } from "async-mutex";
import { injectable, unmanaged } from "inversify";
import { Collection, Db, MongoClient } from "mongodb";
import { Logger } from "tslog";
import { DatabaseConfigMongoDb } from "./databaseConfigMongoDb";
import { DatabaseProviderInterface } from "./databaseProviderInterface";

/**
 * Base class for all mongodb based database providers
 */
@injectable()
export abstract class DatabaseProviderBase<T> implements DatabaseProviderInterface {
    private _mutex: Mutex = new Mutex();

    private _logger!: Logger;
    public get logger(): Logger { return this._logger }
    
    /**
     * Constructor
     * @param _configService config service reference
     * @param _providerName  name for debugging
     * @param _collectionName name of the database collection
     */
    constructor(@unmanaged() private _configService: ConfigService, @unmanaged()  private _providerName: string, @unmanaged()  private _collectionName: string, @unmanaged() _botLogger: BotLogger)
    {
        this._logger = _botLogger.getChildLogger(_providerName);
        this.createIndex().then(() => this.logger.info(`Created index for ${_collectionName}.`));}

    protected get mutex(): Mutex { return this._mutex; }
    protected get collectionName(): string { return this._collectionName; }
    public get providerName(): string { return this._providerName; }

    /**
     * Connect to MongoDB instance
     * @returns client instance
     */
    protected async connect(): Promise<MongoClient> {
        const databaseConfig = this._configService.getConfig<DatabaseConfigMongoDb>('mongoDB');
        if (databaseConfig == undefined) {
            throw 'DatabaseConfigMongoDb must be added to config service with key mongoDB.';
        }
        return await MongoClient.connect(`mongodb://${databaseConfig.databaseUser}:${databaseConfig.databasePassword}@${databaseConfig.databaseUrl}?authMechanism=SCRAM-SHA-256&authSource=${databaseConfig.authSource}`);
    }

    /**
     * Open database on MongoDB instance
     * @param client connected instance
     * @returns database instance
     */
    protected async openDatabase(client: MongoClient): Promise<Db> {
        const databaseConfig = this._configService.getConfig<DatabaseConfigMongoDb>('mongoDB');
        if (databaseConfig == undefined) {
            throw 'DatabaseConfigMongoDb must be added to config service with key mongoDB.';
        }
        return client.db(databaseConfig.databaseName);
    }

    /**
     * Open collection on MongoDB instance
     * @param client connected instance
     * @returns collection instance
     */
    protected async openCollection(client: MongoClient): Promise<Collection<T>> {
        const db = await this.openDatabase(client);
        const collection = db.collection<T>(this.collectionName);
        return collection;
    }

    /**
     * Store data object
     * @param data 
     * @param overwriteIfExists 
     * @returns 
     */
    public async store(data: T, overwriteIfExists: boolean): Promise<boolean> {
        const release = await this.mutex.acquire();
        try {
            return this.storeCore(data, overwriteIfExists);
        } finally {
            release();
        }
    }

    /**
     * Abstract core, must be implemented in sub classes
     */
    protected abstract storeCore(data: T, overwriteIfExists: boolean): Promise<boolean>;
    
    /**
     * Returns true of object exists in database
     * @param data 
     * @returns 
     */
    public async exists(data: T): Promise<boolean> {
        return this.existsCore(data);
    }

    /**
     * Abstract core, must be implemented in sub classes
     */
    protected abstract existsCore(data: T): Promise<boolean>;

    /**
     * Deletes object from database
     * @param data 
     * @returns 
     */
    public async delete(data: T): Promise<number> {
        const release = await this.mutex.acquire();
        try {
            return this.deleteCore(data);
        } finally {
            release();
        }
    }

    /**
     * Abstract core, must be implemented in sub classes
     */
    protected abstract deleteCore(data: T): Promise<number>;
    
    /**
     * Deletes all objects for the specified guild
     */
    public async deleteAll(guildId: string): Promise<number> {
        const release = await this.mutex.acquire();
        try {
            return this.deleteAllCore(guildId);
        } finally {
            release();
        }
    }

    /**
     * Abstract core, must be implemented in sub classes
     */
    protected abstract deleteAllCore(guildId: string): Promise<number>;

    /**
     * Create an index on this provider's collection
     * @returns 
     */
    public async createIndex(): Promise<void> {
        const release = await this.mutex.acquire();
        try {
            return this.createIndexCore();
        } catch(ex) {
            this.logger.error(ex);
        } finally {
            release();
        }
    }

    /**
     * Abstract core, must be implemented in sub classes
     */
    protected abstract createIndexCore(): Promise<void>;
}